from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context
import st3m.run
import leds

red = [255,0,0]
black = [0,0,0]
color = red

rect_size = 10

logo = [
    [0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1],
    [1,1,0,1,0,0,1,1,0,1,1,0,1,1,0,1],
    [1,1,0,1,0,0,1,1,0,1,1,0,1,1,0,1],
    [0,1,1,1,0,0,0,1,0,0,1,0,0,1,1,1],
    [0,1,1,1,0,0,1,1,1,1,1,0,0,1,1,1],
    [0,0,1,0,0,1,1,1,1,1,1,1,0,0,1,0],
    [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0],
    [0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0],
    [0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0],
    [0,0,0,1,0,0,0,1,1,1,0,0,0,1,0,0],
    [0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,0],
    [0,0,1,0,0,1,0,1,0,1,0,1,0,0,1,0],
    [0,0,1,0,1,1,0,1,0,1,0,1,1,0,1,0]
]

shift_y = -1 * len(logo) * rect_size / 2
shift_x = -1 * len(logo[0]) * rect_size / 2 - rect_size 


class Krebs(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

    def draw(self, ctx: Context) -> None:
        ctx.rgb(black[0], black[1], black[2]).rectangle(-120, -120, 240, 240).fill()
        ctx.translate(shift_x, shift_y)

        for j in range(0, len(logo)):
            line = logo[j]

            for i in range(0, len(line)):
                if line[i]: 
                    color = red
                else: 
                    color = black

                ctx.rgb(color[0], color[1], color[2]).translate(rect_size, 0).rectangle(0,0, rect_size, rect_size).fill()

            ctx.translate(-1 * rect_size * len(line), rect_size)    

        leds.set_all_rgb(red[0],red[1],red[2])
        leds.update()    

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing


if __name__ == '__main__':
    st3m.run.run_view(Krebs(ApplicationContext()))